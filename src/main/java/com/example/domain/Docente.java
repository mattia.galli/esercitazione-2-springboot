package com.example.domain;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name="docenti")

@Getter @Setter @ToString
public class Docente {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "codide_docente")
    private Long codice_docente;

    @Column(name = "cognome")
    private String cognome;

    @Column(name = "nome")
    private String nome;

    @Column(name = "qualifica")
    private String qualifica;

    @Column(name = "materia")
    private String materia;

    @ManyToMany(fetch = FetchType.LAZY, cascade = {
        CascadeType.MERGE,
        CascadeType.PERSIST
    })
    @JoinTable(name = "classe", 
        joinColumns = @JoinColumn(name = "sigla_classe"),
        inverseJoinColumns = @JoinColumn(name = "codice_docente")
    )
    @JsonIgnore
    private Set<Docente> docenti;
    
}
