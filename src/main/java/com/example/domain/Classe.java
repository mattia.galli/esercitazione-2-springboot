package com.example.domain;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "classi")
@Getter @Setter @ToString
public class Classe {
    @Id
    @Column(name = "sigla_classe")
    private String sigla_classe;

    @Column(name = "numero_alunni")
    private Long numero_alunni;

    @Column(name = "aula")
    private Long aula;

    @ManyToMany(fetch = FetchType.LAZY, cascade = {
        CascadeType.MERGE,
        CascadeType.PERSIST
    })
    @JoinTable(name = "docente", 
        joinColumns = @JoinColumn(name = "codice_docente"),
        inverseJoinColumns = @JoinColumn(name = "sigla_classe")
    )
    @JsonIgnore
    private Set<Classe> classi;
}
