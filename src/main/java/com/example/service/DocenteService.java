package com.example.service;

import java.util.List;
import java.util.Optional;

import com.example.domain.Docente;
import com.example.repository.DocenteRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
public class DocenteService {
    @Autowired //dependency injection, istanzia linterfaccia in un attributo (come se fosse un new)
    DocenteRepository docenteRepository;

    public List<Docente> findAll(){
        return docenteRepository.findAll();
    }

    public Optional<Docente> findById(long codice_docente){
        return docenteRepository.findById(codice_docente);
    }

    public void save(Docente docente){
        docenteRepository.save(docente);
    }

    public Optional<Docente> deleteById(Long codice_docente){
        Optional<Docente> otpDocente =  docenteRepository.findById(codice_docente);
        if (otpDocente.isPresent()){
            docenteRepository.deleteById(codice_docente);
            return otpDocente;
        }
        return Optional.empty();
    }

    public Optional<Docente> put(Docente attore){
        Optional<Docente> otpDocente= docenteRepository.findById(attore.getCodice_docente());
    
        if(otpDocente.isPresent()){
            docenteRepository.save(attore);
            return otpDocente;
        }
            return Optional.empty();
    }

    public List<Docente> findAllPage(PageRequest request){
        return docenteRepository.findAll(request).getContent();
    }


    
}
