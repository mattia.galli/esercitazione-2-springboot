package com.example.service;

import java.util.List;
import java.util.Optional;

import com.example.domain.Classe;

import com.example.repository.ClasseRepository;

import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
public class ClasseService {
    ClasseRepository classeRepository;

    public List<Classe> findAll(){ 
        return classeRepository.findAll();
    }

    public Optional<Classe> findById(String sigla_classe){
        return classeRepository.findById(sigla_classe);
    }

    public void save(Classe classe){
        classeRepository.save(classe);
    }

    public Optional<Classe> deleteById(String sigla_classe){
        Optional<Classe> optClasse =  classeRepository.findById(sigla_classe);
        if (optClasse.isPresent()){
            classeRepository.deleteById(sigla_classe);
            return optClasse;
        }
        return Optional.empty();
    }

    public Optional<Classe> put(Classe classe){
        Optional<Classe> optClasse= classeRepository.findById(classe.getSigla_classe());
    
        if(optClasse.isPresent()){
            classeRepository.save(classe);
            return optClasse;
        }
            return Optional.empty();
    }

    public List<Classe> findAllPage(PageRequest request){
        return classeRepository.findAll(request).getContent();
    }
}
