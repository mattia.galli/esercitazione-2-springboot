package com.example.controller;

import java.util.List;
import java.util.Optional;

import com.example.domain.Docente;
import com.example.service.DocenteService;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("(api/docenti")
@CrossOrigin(origins = "*", maxAge = 3600)
public class DocenteController {

    DocenteService docenteService;

    public DocenteController (DocenteController docenteController) {
        this.docenteService = docenteService;
    }

    @GetMapping()
    public ResponseEntity<?> findAll(){
        return new ResponseEntity<List<Docente>>(docenteService.findAll(), HttpStatus.OK);

    }

    @GetMapping("/{codice_docente}")
    public ResponseEntity<?> findById(@PathVariable("codice_docente") Long codice_docente){

        Optional<Docente> optDocente = docenteService.findById(codice_docente);
        if (optDocente.isPresent()){
            Docente docente = optDocente.get();
            return new ResponseEntity<Docente>(docente,HttpStatus.OK);
        }
        return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
    }

    @PostMapping()
    public ResponseEntity<?> save(@RequestBody Docente docente){
        docenteService.save(docente);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }


    
}
